#include <Rcpp.h>
using namespace Rcpp;
#include <math.h>

// [[Rcpp::export]]
double cPhytomer_initiation_rate_fun(double Surface_area, double PAR_umol_s, double Daylength, double min_PIR, double max_PIR){
	// from Chenu et al 2005 Fig 5.
	double absorbed_PAR = 0;
	double PIR = 0;

	absorbed_PAR = PAR_umol_s * Surface_area * 60*60 * Daylength / 1000; 	// take surface area and daylength and convert to units for Chenu
	PIR = 28.1e-3 * log10(absorbed_PAR) + 77.5e-3;

	PIR = std::min(std::max(PIR,min_PIR),max_PIR); 
	return PIR;
}



// [[Rcpp::export]]
double cSLA_by_TT(double TT){ 		// Fig 3b.
	// TT is in thermo_time since emergence
	// equation in paper appears to have wrong units. By Fig 4, a leaf with mass 0.005g has Leaf area 3cm^2 = 0.0003m^2, or SLA = 0.06. 
	// But Equation gives SLA~6.
	double correction_factor = 100.;
	double SLA;

	SLA = 14.4 * exp(-0.002*TT) / correction_factor;
	return SLA;
}

// [[Rcpp::export]]
NumericVector cleaf_alpha(NumericVector leaf_biomass, int i_last) {		// equation 5.1
	// return alpha in radians
	NumericVector alpha(leaf_biomass.size(),90.);
	int i_max = 0;
	double max_biomass = 0.;

	for (int i = 0; i <= i_last; i++) {
		if(leaf_biomass[i] > max_biomass){
			i_max = i;
			max_biomass = leaf_biomass[i];
		}
  	}
	for (int i = 0; i <= i_last; i++) {
		if(i <= i_max){
			alpha[i] = 10;
		} else{
			alpha[i] = 60 * (i-i_max)/(i_last-i_max) + 10;
		}
	}
	alpha = alpha * M_PI/180;
	return alpha;	
}

// [[Rcpp::export]]
NumericVector cdemand_function_vector(NumericVector j,double To,double ao,double bo){
	int n = j.size();
	NumericVector demand(n);
	NumericVector stdAge(n);
	double maxDemand = R::dbeta((ao-1)/(ao+bo-2),ao,bo,0);

	stdAge = (j+0.5)/To;
	demand = 1/maxDemand * dbeta(stdAge,ao,bo,0);

	return demand;
}

// [[Rcpp::export]]
double cdemand_function_scalar(double j,double To,double ao,double bo){
	double stdAge,demand;
	double maxDemand = R::dbeta((ao-1)/(ao+bo-2),ao,bo,0);

	stdAge = (j+0.5)/To;
	demand = 1/maxDemand * R::dbeta(stdAge,ao,bo,0);

	return demand;
}


// leaf length. Approximates leaf as an ellipse with a particular ratio of the two axes, given thermal time and mass
// [[Rcpp::export]]
NumericVector cleaf_length(NumericVector mass, double TT, double width_length_ratio = 0.5){
	// returns mm
	int n = mass.size();
	NumericVector area(n);
	NumericVector length(n);  
	area = cSLA_by_TT(TT) * mass;		
	length = sqrt(4*area/(M_PI*width_length_ratio)) * 1e3;
	return length;
}



//# -------- Begin model ---------- #
// [[Rcpp::export]]
List c_run_mod_Plastochron_model(	int nCycles,
						Function RUE_per_phytomer,
						List parameters,
						double PAR_umol_s,
						double Daylength               
					   ) {
	// Extract parameters
	List sink_strengths              = as<List>(parameters["sink_strengths"]);
	double min_PIR                   = as<double>(parameters["min_PIR"]);
	double max_PIR                   = as<double>(parameters["max_PIR"]);
	double TT_emergence              = as<double>(parameters["TT_emergence"]);
	double Q0                        = as<double>(parameters["Q0"]);
	double TU_per_day                = as<double>(parameters["TU_per_day"]);
	double leaf_lifespan             = as<double>(parameters["leaf_lifespan"]);
	double leaf_delay                = as<double>(parameters["leaf_delay"]);
	double reproductive_transtion_TT = as<double>(parameters["reproductive_transtion_TT"]);

	double PAR = PAR_umol_s * Daylength * 60*60 / 8*1.6 / 1e6;		// convert back to MJ/m^2/d

	// initialize variables to store timecourses
	NumericMatrix leaf_demand(nCycles,nCycles),internode_biomass(nCycles,nCycles),leaf_biomass(nCycles,nCycles);
	NumericVector root_biomass(nCycles),Q_j(nCycles),D_j(nCycles),S_j(nCycles);

	// initialize state variables 
	NumericVector current_leaf_biomass(nCycles);
	NumericVector current_internode_biomass(nCycles);
	double current_root_biomass      = 0;
	double cum_TT                    = 0;
	double Q;
	double Surface_area;
	List sink;
	double strength,To,ao,bo;
	double demand_root;
	NumericVector demand_leaves(nCycles,0.),demand_internodes(nCycles,0.);
	double total_demand;

	NumericVector current_leaf_TT(nCycles,0.); 		// vector that holds the age in TT of each leaf
	NumericVector current_internode_TT(nCycles,0.); 	//vector that holds the age in TT of each leaf	
	NumericVector TT_by_phytomer(nCycles,0.);

	LogicalVector green_leaves(nCycles,FALSE);
	NumericVector green_biomass(nCycles,0.);
	NumericVector green_surface_area(nCycles,0.);
	NumericVector Si(nCycles,0.);

	TT_by_phytomer[0] = 1./cPhytomer_initiation_rate_fun(0,PAR_umol_s,Daylength,min_PIR,max_PIR);

	int n_Rosette_phytomer = 3;  	// Initial guess
	int n_Total_leaves = nCycles;

	// run simulation	
	for (int j = 0; j < nCycles; j++) {

		// during this phytomer, we: 
			// 1) calculate plastochron length and update RLN and TLN
			// 2) calculate existing green leaf areas, 
			// 3) calculate total supply, 
			// 4) calculate individual demand, 
			// 5) allocate new biomass


		// determine if reproductive_transition has occurred. If not, keep adding rosette leaves
		if(cum_TT + TT_by_phytomer[j] < reproductive_transtion_TT) n_Rosette_phytomer = std::max(3,j+1);

		// determine state of the plant in terms of total age, SLA and RUE
		double SLA = cSLA_by_TT(std::max(0.,cum_TT-TT_emergence));
		List RUE_l = RUE_per_phytomer(std::max(0.,cum_TT-TT_emergence));
		double RUE = as<double>(RUE_l["RUE"]);

		// set up new phytomer. Phytomer j has age 0 at cycle j. Is this right, or should it have age TT_by_phytomer[j]?
			// internodes produced before reproductive transition don't age, so their age will stay at NA
		green_leaves[j]              = TRUE;
		current_leaf_biomass[j]      = 0;
		current_internode_biomass[j] = 0;
		current_leaf_TT[j]           = 0;
		current_internode_TT[j]      = 0;

		// calculate supply function (Q)
			// if first cycle, Q = Q0.
			// otherwise, supply determined by vertically projected leaf area
		if(j == 0){
			Q = Q0;
			Surface_area = 0;
		} else{

			// calculate vertical green area	
			green_leaves[current_leaf_TT > leaf_lifespan] = FALSE;
			// green_biomass = current_leaf_biomass; 	// how massive is each leaf?
			// green_biomass[!green_leaves] = 0.;
			green_surface_area = SLA * current_leaf_biomass * cos(cleaf_alpha(current_leaf_biomass,j)); 	// what is the vertical surface area of each leaf?
			green_surface_area[!green_leaves] = 0.;

			// find set of 12 consecutive leaves with greatest surface area
			for (int k=0; k < j; k++){
				Si[k] = sum(green_surface_area[seq(k,std::min(k+12,j))]);
			}
			Surface_area = max(Si);		
	
			// calculate supply. PAR is in MJ/m^2/d, so dividing by T_per_day cancels the /d units.
			Q = RUE * Surface_area * PAR * TT_by_phytomer[j] / TU_per_day;
		}

			

		// calculate the demands of each organ type
			// root has age == plant age
		sink = as<List>(sink_strengths["Root"]);
		strength = as<double>(sink["strength"]);
		To       = as<double>(sink["To"]);
		ao       = as<double>(sink["ao"]);
		bo       = as<double>(sink["bo"]);
		if(j == 0){
			demand_root = strength * cdemand_function_scalar(0.,To,ao,bo);
		} else{
			demand_root = strength * cdemand_function_scalar(cum_TT-leaf_delay,To,ao,bo);
		}
			

			// there are three types of leaves:
				// 2 cotyledons
				// regulat Rosette leaves
				// post-bolting leaves (cauline)
		sink = as<List>(sink_strengths["Cotyledon_leaf"]);
		strength = as<double>(sink["strength"]);
		To       = as<double>(sink["To"]);
		ao       = as<double>(sink["ao"]);
		bo       = as<double>(sink["bo"]);
		if(j == 0){
			demand_leaves[0] = strength * cdemand_function_scalar(0.,To,ao,bo);
		} else{
			demand_leaves[seq(0,1)] = strength * cdemand_function_vector(current_leaf_TT[seq(0,1)]-leaf_delay,To,ao,bo);

			if(j > 1){
				sink = as<List>(sink_strengths["Rosette_leaf"]);
				strength = as<double>(sink["strength"]);
				To       = as<double>(sink["To"]);
				ao       = as<double>(sink["ao"]);
				bo       = as<double>(sink["bo"]);
				demand_leaves[seq(2,n_Rosette_phytomer-1)] = strength * cdemand_function_vector(current_leaf_TT[seq(2,n_Rosette_phytomer-1)]-leaf_delay,To,ao,bo);
			}
			

			if(j >= n_Rosette_phytomer){
				sink = as<List>(sink_strengths["Inflorescence_leaf"]);
				strength = as<double>(sink["strength"]);
				To       = as<double>(sink["To"]);
				ao       = as<double>(sink["ao"]);
				bo       = as<double>(sink["bo"]);
				demand_leaves[seq(n_Rosette_phytomer,n_Total_leaves-1)] = strength * cdemand_function_vector(current_leaf_TT[seq(n_Rosette_phytomer,n_Total_leaves-1)]-leaf_delay,To,ao,bo);
			}
		}
		// demand_leaves[!green_leaves] = 0.;
		// demand_leaves[seq(0,nCycles-1) > n_Total_leaves] = 0.;
	

		// internodes produced before reproductive transition have no demand
		sink = as<List>(sink_strengths["Internode"]);
		strength = as<double>(sink["strength"]);
		To       = as<double>(sink["To"]);
		ao       = as<double>(sink["ao"]);
		bo       = as<double>(sink["bo"]);
		demand_internodes = strength * cdemand_function_vector(current_internode_TT,To,ao,bo);
	
		for(int k = 0; k < nCycles; k++){
			if(!green_leaves[k]) demand_leaves[k] = 0.;
			if(k <= n_Rosette_phytomer-1) demand_internodes[k] = 0.;
			if(k > j) {
				demand_leaves[k] = 0.;
				demand_internodes[k] = 0.;
			}
		}
	
		total_demand = demand_root + sum(demand_leaves) + sum(demand_internodes);
		if(total_demand == 0.) total_demand = 1.;

	
		// allocate C to each organ
		current_root_biomass      = current_root_biomass + demand_root * Q / total_demand;
		current_leaf_biomass      = current_leaf_biomass + demand_leaves * Q / total_demand;
		current_internode_biomass = current_internode_biomass + demand_internodes * Q / total_demand;

	
		// store current state in time series
		Q_j[j] = Q / ( TT_by_phytomer[j] / mean(TT_by_phytomer)); 	// normalize Q roughly / day because of variable length phytomers
		D_j[j] = total_demand;
		S_j[j] = Surface_area;
		leaf_biomass(_,j) = current_leaf_biomass;
		internode_biomass(_,j) = current_internode_biomass;
		leaf_demand(_,j) = demand_leaves;
		root_biomass[j] = current_root_biomass;

		// increment ages of leaves and internodes
		cum_TT = sum(TT_by_phytomer);
		current_leaf_TT[seq(0,j)] = current_leaf_TT[seq(0,j)] + TT_by_phytomer[j];
		if(cum_TT >= reproductive_transtion_TT){
			current_internode_TT[seq(n_Rosette_phytomer-1,j)] = current_internode_TT[seq(n_Rosette_phytomer-1,j)] + TT_by_phytomer[j];
		}

		// Determine the next plastochron length		
		TT_by_phytomer[j+1] = 1./cPhytomer_initiation_rate_fun(Surface_area,PAR_umol_s,Daylength,min_PIR,max_PIR);	
	
	}

	return List::create(_["leaf_biomass"]=leaf_biomass,
						_["internode_biomass"]=internode_biomass,
						_["root_biomass"]=root_biomass,
						_["TT_by_phytomer"] = TT_by_phytomer,
						_["Q_j"] = Q_j,
						_["D_j"] = D_j,
						_["S_j"] = S_j,
						_["leaf_demand"] = leaf_demand);
}
	/*
	

	

	
		// allocate C to each organ
		current_root_biomass           = current_root_biomass + demand_root * Q / total_demand
		current_leaf_biomass[1:j]      = current_leaf_biomass[1:j] + demand_leaves * Q / total_demand
		current_internode_biomass[1:j] = current_internode_biomass[1:j] + demand_internodes * Q / total_demand

		// store current state in time series
		Q_j[j] = Q / ( TT_by_phytomer[j] / mean(TT_by_phytomer)) 	// normalize Q roughly / day because of variable length phytomers
		D_j[j] = total_demand 
		S_j[j] = Surface_area
		leaf_biomass[1:j,j] = current_leaf_biomass	
		internode_biomass[1:j,j] = current_internode_biomass	
		leaf_demand[1:j,j] = demand_leaves	
		root_biomass[j] = current_root_biomass

		// increment ages of leaves and internodes
		cum_TT = sum(TT_by_phytomer[1:j])
		current_leaf_TT = current_leaf_TT + TT_by_phytomer[j]
		current_internode_TT = current_internode_TT + TT_by_phytomer[j]

		// Determine the next plastochron length		
		TT_by_phytomer[j+1] = 1/Phytomer_initiation_rate_fun(Surface_area,PAR_umol_s,Daylength)
	}

	output = list()
	output$leaf_biomass      = leaf_biomass
	output$internode_biomass = internode_biomass
	output$root_biomass      = root_biomass
	output$TT_by_phytomer    = TT_by_phytomer
	output$Q_j               = Q_j
	output$D_j               = D_j
	output$S_j               = S_j
	output$leaf_demand       = leaf_demand

	return(output)
}

*/