#include <Rcpp.h>
using namespace Rcpp;
#include <math.h>

// [[Rcpp::export]]
double cPhytomer_initiation_rate_fun(double Surface_area, double PAR_integral, double min_PIR, double max_PIR){
	// from Chenu et al 2005 Fig 5.
	double absorbed_PAR = 0;
	double PIR = 0;

	absorbed_PAR = PAR_integral * Surface_area / 1000; 	// take surface area and daylength and convert to units for Chenu
	PIR = 28.1e-3 * log10(absorbed_PAR) + 77.5e-3;

	PIR = std::min(std::max(PIR,min_PIR),max_PIR); 
	return PIR;
}



// [[Rcpp::export]]
double cSLA_by_TT(double TT){ 		// Fig 3b.
	// TT is in thermo_time since emergence
	// equation in paper appears to have wrong units. By Fig 4, a leaf with mass 0.005g has Leaf area 3cm^2 = 0.0003m^2, or SLA = 0.06. 
	// But Equation gives SLA~6.
	double correction_factor = 100.;
	double SLA;

	SLA = 14.4 * exp(-0.002*TT) / correction_factor;
	return SLA;
}

// [[Rcpp::export]]
NumericVector cleaf_alpha(NumericVector leaf_biomass, int i_last) {		// equation 5.1
	// return alpha in radians
	NumericVector alpha(leaf_biomass.size(),90.);
	int i_max = 0;
	double max_biomass = 0.;

	for (int i = 0; i <= i_last; i++) {
		if(leaf_biomass[i] > max_biomass){
			i_max = i;
			max_biomass = leaf_biomass[i];
		}
  	}
	for (int i = 0; i <= i_last; i++) {
		if(i <= i_max){
			alpha[i] = 10;
		} else{
			alpha[i] = 60 * double(i-i_max)/(i_last-i_max) + 10;
		}
	}
	alpha = alpha * M_PI/180;
	return alpha;	
}

// [[Rcpp::export]]
NumericVector cdemand_function_vector(NumericVector j,double To,double ao,double bo){
	int n = j.size();
	NumericVector demand(n);
	NumericVector stdAge(n);
	double maxDemand = R::dbeta((ao-1)/(ao+bo-2),ao,bo,0);

	stdAge = (j+0.5)/To;
	demand = 1/maxDemand * dbeta(stdAge,ao,bo,0);

	return demand;
}

// [[Rcpp::export]]
double cdemand_function_scalar(double j,double To,double ao,double bo){
	double stdAge,demand;
	double maxDemand = R::dbeta((ao-1)/(ao+bo-2),ao,bo,0);

	stdAge = (j+0.5)/To;
	demand = 1/maxDemand * R::dbeta(stdAge,ao,bo,0);

	return demand;
}


// leaf length. Approximates leaf as an ellipse with a particular ratio of the two axes, given thermal time and mass
// [[Rcpp::export]]
NumericVector cleaf_length(NumericVector mass, double TT, double width_length_ratio = 0.5){
	// returns mm
	int n = mass.size();
	NumericVector area(n);
	NumericVector length(n);  
	area = cSLA_by_TT(TT) * mass;		
	length = sqrt(4*area/(M_PI*width_length_ratio)) * 1e3;
	return length;
}


struct sink_parameters {
	double strength, To, ao, bo;
};	

double c_calc_root_demand(double age_TT, sink_parameters Root_sink){
	double demand_root = Root_sink.strength * cdemand_function_scalar(age_TT,Root_sink.To,Root_sink.ao,Root_sink.bo);

	return demand_root;
}

NumericVector c_calc_leaf_demand(NumericVector age_TT,sink_parameters Cotyledon_sink,sink_parameters Rosette_sink){
	int nLeaves = age_TT.size();
	NumericVector demand_leaves(nLeaves,0.);

	// Cotyledons
	demand_leaves[seq(0,1)] = Cotyledon_sink.strength * cdemand_function_vector(age_TT[seq(0,1)],Cotyledon_sink.To,Cotyledon_sink.ao,Cotyledon_sink.bo);

	// Rosette
	demand_leaves[seq(2,nLeaves-1)] = Rosette_sink.strength * cdemand_function_vector(age_TT[seq(2,nLeaves-1)],Rosette_sink.To,Rosette_sink.ao,Rosette_sink.bo);

	return demand_leaves;
}

// // [[Rcpp::export]]
// NumericVector c_calc_leaf_demand(NumericVector age_TT,List sink_strengths){
// 	int nLeaves = age_TT.size();
// 	NumericVector demand_leaves(nLeaves,0.);
// 	List sink;
// 	double strength,To,ao,bo;

// 	// Cotyledons
// 	sink = as<List>(sink_strengths["Cotyledon_leaf"]);
// 	strength = as<double>(sink["strength"]);
// 	To       = as<double>(sink["To"]);
// 	ao       = as<double>(sink["ao"]);
// 	bo       = as<double>(sink["bo"]);
// 	demand_leaves[seq(0,1)] = strength * cdemand_function_vector(age_TT[seq(0,1)],To,ao,bo);

// 	// Rosette
// 	sink = as<List>(sink_strengths["Rosette_leaf"]);
// 	strength = as<double>(sink["strength"]);
// 	To       = as<double>(sink["To"]);
// 	ao       = as<double>(sink["ao"]);
// 	bo       = as<double>(sink["bo"]);
// 	demand_leaves[seq(2,nLeaves-1)] = strength * cdemand_function_vector(age_TT[seq(2,nLeaves-1)],To,ao,bo);

// 	return demand_leaves;
// }

// [[Rcpp::export]]
double c_allocate_biomass_scalar(double demand,double Q_over_D){
	return demand * Q_over_D;
}

// [[Rcpp::export]]
NumericVector c_allocate_biomass_vector(NumericVector demand,double Q_over_D){
	return demand * Q_over_D;
}


struct Q_SA_result {
	double Q;
	double Surface_area;
};

Q_SA_result c_calc_Q_SA(	
					LogicalVector green_leaves,
					NumericVector current_leaf_TT, 
					double leaf_lifespan,
					NumericVector current_leaf_biomass,
					int cycle,
					double SLA,
					double RUE,
					double PAR_integral
				){

	// Calc vertical surface area of each leaf
	NumericVector green_surface_area = SLA * current_leaf_biomass * cos(cleaf_alpha(current_leaf_biomass,cycle)); 	
	green_surface_area[!green_leaves] = 0.;

	NumericVector Si(cycle,0.);

	// find set of 12 consecutive leaves with greatest surface area
	for (int k=0; k < cycle; k++){
		Si[k] = sum(green_surface_area[seq(k,std::min(k+12,cycle))]);
	}
	double Surface_area = max(Si);		

	// calculate supply. PAR is in MJ/m^2/d, so dividing by T_per_day cancels the /d units.
	double Q = RUE * Surface_area * PAR_integral / 8.*1.6 / 1.e6;

	Q_SA_result Q_SA;
	Q_SA.Q = Q;
	Q_SA.Surface_area = Surface_area;

	return Q_SA;

	// return List::create(_["Q"] 			 = double(Q),
	// 					_["Surface_area"]= double(Surface_area));
}

// [[Rcpp::export]]
int c_develop_plant_day(
							double TT_day,
							int cycle,
							NumericVector TT_by_phytomer,
							NumericVector current_leaf_TT,
							NumericVector current_leaf_biomass,
							LogicalVector green_leaves,
							double leaf_lifespan,
							int nCycles
						) {
			double TT_since_last_leaf = current_leaf_TT[cycle];

			// The age of each existing leaf is incremented by TT_day
			current_leaf_TT[seq(0,cycle)] = current_leaf_TT[seq(0,cycle)] + TT_day;
			green_leaves[current_leaf_TT > leaf_lifespan] = FALSE;

			// add leaves and age them 

			double TT_to_next_phytomer = TT_by_phytomer[cycle]-TT_since_last_leaf;		// TT neaded for next leaf
			double TT_day_remainder = TT_day;											// TT remaining in day to allocate

			while(TT_day_remainder > TT_to_next_phytomer){
				// Move TT to next phytomer
				TT_day_remainder = TT_day_remainder - TT_to_next_phytomer;

				// add new phytomer
				cycle = cycle + 1;
				if(cycle >= nCycles) break;

				// initialize phytomer
				green_leaves[cycle] = TRUE;
				current_leaf_biomass[cycle] = 0;
				current_leaf_TT[cycle] = TT_day_remainder;
				TT_by_phytomer[cycle] = TT_by_phytomer[cycle-1];

				// calc time to next phytomer
				TT_to_next_phytomer = TT_by_phytomer[cycle];
			}
			return cycle;
			// return List::create(
			// 			_["cycle"]                = cycle,
			// 			_["TT_by_phytomer"]       = TT_by_phytomer,
			// 			_["current_leaf_TT"]      = current_leaf_TT,
			// 			_["current_leaf_biomass"] = current_leaf_biomass,
			// 			_["green_leaves"]         = green_leaves
			// 			);
}
	

//# -------- Begin model ---------- #
// [[Rcpp::export]]
List c_sim_vegetative_development(
						int nDays,
						int nCycles,
						Function RUE_per_phytomer,
						List parameters,
						NumericVector TT_per_hour,
						NumericVector PAR_umol_h
					   ) {
	// Extract parameters
	List sink_strengths = as<List>(parameters["sink_strengths"]);
	sink_parameters Root_sink, Cotyledon_sink, Rosette_sink;
	List sink;
	sink = as<List>(sink_strengths["Root"]);
	Root_sink.strength      = as<double>(sink["strength"]);
	Root_sink.To            = as<double>(sink["To"]);
	Root_sink.ao            = as<double>(sink["ao"]);
	Root_sink.bo            = as<double>(sink["bo"]);
	sink = as<List>(sink_strengths["Cotyledon_leaf"]);
	Cotyledon_sink.strength = as<double>(sink["strength"]);
	Cotyledon_sink.To       = as<double>(sink["To"]);
	Cotyledon_sink.ao       = as<double>(sink["ao"]);
	Cotyledon_sink.bo       = as<double>(sink["bo"]);
	sink = as<List>(sink_strengths["Rosette_leaf"]);
	Rosette_sink.strength   = as<double>(sink["strength"]);
	Rosette_sink.To         = as<double>(sink["To"]);
	Rosette_sink.ao         = as<double>(sink["ao"]);
	Rosette_sink.bo         = as<double>(sink["bo"]);

	double min_PIR                   = as<double>(parameters["min_PIR"]);
	double max_PIR                   = as<double>(parameters["max_PIR"]);
	double TT_emergence              = as<double>(parameters["TT_emergence"]);
	double Q0                        = as<double>(parameters["Q0"]);
	double leaf_lifespan             = as<double>(parameters["leaf_lifespan"]);
	double leaf_delay                = as<double>(parameters["leaf_delay"]);


	// initialize variables to store timecourses
	NumericMatrix leaf_demand(nCycles,nDays),internode_biomass(nCycles,nDays),leaf_biomass(nCycles,nDays);
	NumericVector root_biomass(nDays),TT_by_day(nDays),Q_day(nDays),D_day(nDays),S_day(nDays);

	// initialize state variables 
	NumericVector current_leaf_biomass(nCycles,0.);
	NumericVector current_internode_biomass(nCycles,0.);
	double current_root_biomass = 0;
	double cum_TT               = 0;
	double Q                    = 0;
	double Surface_area         = 0;
	// List sink;
	// double strength,To,ao,bo;
	NumericVector demand_leaves(nCycles,0.),demand_internodes(nCycles,0.);
	double demand_root  		= 0;
	double total_demand 		= 0;

	NumericVector current_leaf_TT(nCycles,0.); 		// vector that holds the age in TT of each leaf
	NumericVector current_internode_TT(nCycles,0.); 	//vector that holds the age in TT of each leaf	
	NumericVector TT_by_phytomer(nCycles,0.);

	LogicalVector green_leaves(nCycles,FALSE);
	NumericVector green_biomass(nCycles,0.);
	NumericVector green_surface_area(nCycles,0.);
	NumericVector Si(nCycles,0.);

	TT_by_phytomer[0] = 1./cPhytomer_initiation_rate_fun(0.,0.,min_PIR,max_PIR);

	int cycle = 0;

	green_leaves[cycle] = TRUE;

	// run simulation	
	for (int day = 0; day < nDays; day++) {

		// 1) Surface area set at the beginning of the day
		// 2) During day, determine time(s) when leaf initiation begins based on PIR set on previous day. Initiate these leaves
		// 3) At end of day, calculate total C gain and distribute 
		// 4) At same time, calculate total daily light integral, and calculate PIR (leaves / oCd) for following day.


		// determine state of the plant in terms of total age, SLA and RUE
			double SLA = cSLA_by_TT(std::max(0.,cum_TT-TT_emergence));
			NumericVector RUE_l = RUE_per_phytomer(std::max(0.,cum_TT-TT_emergence));
			double RUE = RUE_l[0];

		// Add leaves (cycles) and age organs
			double TT_day = sum(TT_per_hour[seq(day*24,(day+1)*24-1)]);
			cum_TT += TT_day;
			cycle	= c_develop_plant_day(
												TT_day,
												cycle,
												TT_by_phytomer,
												current_leaf_TT,
												current_leaf_biomass,
												green_leaves,
												leaf_lifespan,
												nCycles
											);
			if(cycle >= nCycles) break;

	
		// Calculate Surface area and Daily supply
			double PAR_integral = sum(PAR_umol_h[seq(day*24,(day+1)*24-1)]);
			if(cycle == 0){
				Q = Q0;
				Surface_area = 0;
			} else{
				Q_SA_result Q_SA = c_calc_Q_SA(	green_leaves,
											current_leaf_TT, 
											leaf_lifespan,
											current_leaf_biomass,
											cycle,
											SLA,
											RUE,
											PAR_integral
										);
				Q = Q_SA.Q;
				Surface_area = Q_SA.Surface_area;
			}


		// Calculate organ demand and allocation
			// root has age == plant age);
			if(cycle == 0){
				demand_root = c_calc_root_demand(0.,Root_sink);
			} else{
				demand_root = c_calc_root_demand(cum_TT-leaf_delay,Root_sink);
			}			

			// there are two types of leaves:
				// 2 cotyledons
				// regular Rosette leaves
			if(cycle == 0){
				demand_leaves[0] = c_calc_root_demand(0.,Cotyledon_sink);
			} else{
				demand_leaves = c_calc_leaf_demand(current_leaf_TT-leaf_delay,Cotyledon_sink,Rosette_sink);
			}
		
			for(int k = 0; k < nCycles; k++){
				if(!green_leaves[k]) demand_leaves[k] = 0.;
				if(k > cycle) {
					demand_leaves[k] = 0.;
				}
			}
	
			total_demand = demand_root + sum(demand_leaves);
			if(total_demand == 0.) total_demand = 1.;

		
			// allocate C to each organ
			current_root_biomass      = current_root_biomass + c_allocate_biomass_scalar(demand_root,Q / total_demand);
			current_leaf_biomass      = current_leaf_biomass + c_allocate_biomass_vector(demand_leaves, Q / total_demand);

	
		// Store current state in time series
			TT_by_day[day] = cum_TT;
			Q_day[day]     = Q; 
			D_day[day]     = total_demand;
			S_day[day]     = Surface_area;
			leaf_biomass(_,day) = current_leaf_biomass;
			leaf_demand(_,day) = demand_leaves;
			root_biomass[day] = current_root_biomass;

		// Determine the next plastochron length		
			TT_by_phytomer[cycle] = 1./cPhytomer_initiation_rate_fun(Surface_area,PAR_integral,min_PIR,max_PIR);	
	
	}

	return List::create(_["leaf_biomass"]=leaf_biomass,
						_["root_biomass"]=root_biomass,
						_["TT_by_phytomer"] = TT_by_phytomer,
						_["Q_day"] = Q_day,
						_["D_day"] = D_day,
						_["S_day"] = S_day,
						_["leaf_demand"] = leaf_demand,
						_["TT_by_day"] = TT_by_day);
}
	