

#This implements the Christophe et al 2008 model as well as I can infer from the text, up through some early reproductive growth.
# It still isn't perfect, but is close in some places.

# here, I make the environmental specification more general so that PAR / daylength can be modified on the fly and TT_by_phytomer is updated as it goes


# Set up parameters
Q0 = 1.6e-5			

TT_juvenile = 338 			# oC
Juv_phytomer = 30.3			# oCd
Adult_phytomer = 11.9		# oCd
leaf_lifespan = 560			# oCd
reproductive_transtion_TT = 550			# 0Cd
nCycles = 45

base_temp = 3				# oC
air_temp = 19.3				# oC
TU_per_day = (air_temp - base_temp)

PAR_umol_s = 1.6 * 8/1.6 * 1e6 / 12/60/60 	# mmol / m^2 / s  # This is as used in the Christophe et al dataset.
Daylength = 12								# They used a 12h day

PAR = PAR_umol_s * Daylength *60*60 / 8*1.6 / 1e6		# convert back to MJ/m^2/d



sink_strengths = list(
	Cotyledon_leaf = list(
		strength = 1,
		ao = 3.07,
		bo = 5.59,
		To = 300),	
	Rosette_leaf = list(
		strength = 1,
		ao = 3.07,
		bo = 5.59,
		To = 400),	
	Root = list(
		strength = 2.64,
		ao = 13.03,
		bo = 9.58,
		To = 960),		
	Inflorescence_leaf = list(
		strength = 0.41,
		ao = 3.07,
		bo = 5.59,
		To = 400),
	Internode = list(
		strength = 0.69,
		ao = 2.62,
		bo = 2.98,
		To = 357)	
	)



# SLA decreases with age
SLA_by_TT = function(TT){ #Fig 3b.
	# TT is in thermo_time since emergence
	# equation in paper appears to have wrong units. By Fig 4, a leaf with mass 0.005g has Leaf area 3cm^2 = 0.0003m^2, or SLA = 0.06. 
	# But Equation gives SLA~6.
	correction_factor = 100
	SLA = 14.4 * exp(-0.002*TT) / correction_factor
	return(SLA)
}
# Calculates angles of leaves. Angles are steeper for newer leaves
leaf_alpha = function(leaf_biomass,i_last){		# equation 5.1
	# return alpha in radians
	i = 1:i_last
	i_max = which(leaf_biomass == max(leaf_biomass))
	alpha = rep(10,i_last)
	alpha[i > i_max] = 60*(i[i > i_max]-i_max)/(i_last-i_max) +10
	return(alpha*pi/180)
}
# demand_function
demand_function = function(j,To,ao,bo){
	N = dbeta((ao-1)/(ao+bo-2),ao,bo)
	return(1/N*dbeta((j+0.5)/To,ao,bo))
}

# RUE is dynamic with age. Text says linear interpolation. Changes with Reproductive transition
RUE_per_phytomer = approxfun(x=c(355,406,507,693,980),y=c(2.71,2.71,4.56,1.84,2.37),method='linear',rule=2)

# leaf length. Approximates leaf as an ellipse with a particular ratio of the two axes, given thermal time and mass
leaf_length = function(mass, TT, width_length_ratio = 0.5){
	#returns mm
	area = SLA_by_TT(TT)*mass
	length = sqrt(4*area/(pi*width_length_ratio)) * 1e3
	return(length)
}


# -------- Begin model ---------- #
run_mod_base = function(
						reproductive_transtion_TT,
						RUE_per_phytomer,
						Phytomer_initiation_rate_fun,
						sink_strengths,
						PAR_umol_s,
						Daylength               
					  ) {

	# initialize variables to store timecourses
	leaf_demand = internode_biomass = leaf_biomass = matrix(NA,nCycles,nCycles)
	root_biomass = rep(NA,nCycles)
	Q_j = rep(NA,nCycles)	
	D_j = rep(NA,nCycles)
	S_j  = rep(NA,nCycles)

	# initialize state variables 
	current_leaf_biomass = 0
	current_internode_biomass = 0
	current_root_biomass = 0
	cum_TT = 0
	current_leaf_TT = c() 		# vector that holds the age in TT of each leaf
	current_internode_TT = c() 	# vector that holds the age in TT of each leaf	
	TT_by_phytomer = c()

	n_Rosette_phytomer = 3  	# Initial guess
	n_Total_leaves = nCycles

	# run simulation	
	for(j in 1:nCycles){

		# during this phytomer, we: 
			# 1) calculate plastochron length and update RLN and TLN
			# 2) calculate existing green leaf areas, 
			# 3) calculate total supply, 
			# 4) calculate individual demand, 
			# 5) allocate new biomass

		# determine plastochron
		# calculates cumulative TT indexed by phytomer
		if(cum_TT + Juv_phytomer < TT_juvenile){
			TT_by_phytomer[j] = Juv_phytomer
		} else if(cum_TT > TT_juvenile){
			TT_by_phytomer[j] = Adult_phytomer
		} else{
			TT_by_phytomer[j] = (TT_juvenile - cum_TT) + (1-((TT_juvenile - cum_TT)/Juv_phytomer))*Adult_phytomer
		}

		# determine if reproductive_transition has occurred. If not, keep adding rosette leaves
		if(cum_TT + TT_by_phytomer[j] < reproductive_transtion_TT) n_Rosette_phytomer = j

		# determine state of the plant in terms of total age, SLA and RUE
		SLA = SLA_by_TT(cum_TT)
		RUE = RUE_per_phytomer(cum_TT)

		# set up new phytomer. Phytomer j has age 0 at cycle j. Is this right, or should it have age TT_by_phytomer[j]?
			# internodes produced before reproductive transition don't age, so their age will stay at NA
		current_leaf_biomass[j] = 0
		current_internode_biomass[j] = 0
		current_leaf_TT[j] = 0
		if(cum_TT < reproductive_transtion_TT) { 			
			current_internode_TT[j] = NA
		} else{
			current_internode_TT[j] = 0
		}


		# calculate supply function (Q)
			# if first cycle, Q = Q0.
			# otherwise, supply determined by vertically projected leaf area
		if(j == 1){
			Q = Q0
			S=0
		} else{

			# calculate vertical green area	
			green_leaves = (current_leaf_TT < leaf_lifespan)+0 		# which leaves are alive?
			green_biomass = green_leaves * current_leaf_biomass 	# how massive is each leaf?
			green_surface_area = SLA * green_biomass * cos(leaf_alpha(current_leaf_biomass,j)) 	# what is the vertical surface area of each leaf?

			# find set of 12 consecutive leaves with greatest surface area
			Si = sapply(1:j,function(k) {
					leaf_subset = k:pmin(k+12,j)		
					s=sum(green_surface_area[leaf_subset])
					return(s)
				})
			S = max(Si)

			# calculate supply. PAR is in MJ/m^2/d, so dividing by T_per_day cancels the /d units.
			Q = RUE * S * PAR * TT_by_phytomer[j] / TU_per_day
		}

		# calculate the demands of each organ type
			# root has age == plant age
		demand_root = with(sink_strengths$Root,strength * demand_function(cum_TT,To,ao,bo))

			# there are three types of leaves:
				# 2 cotyledons
				# regulat Rosette leaves
				# post-bolting leaves (cauline)
		demand_leaves = c(
			with(sink_strengths$Cotyledon_leaf,strength * demand_function(current_leaf_TT[1:2],To,ao,bo)),
			with(sink_strengths$Rosette_leaf,strength * demand_function(current_leaf_TT[3:n_Rosette_phytomer],To,ao,bo)),
			with(sink_strengths$Inflorescence_leaf,strength * demand_function(current_leaf_TT[(n_Rosette_phytomer+1):n_Total_leaves],To,ao,bo))	
			)[1:j]
		demand_leaves[current_leaf_TT > leaf_lifespan] = 0 	# too old leaves have no demand (not needed due to demand function)
		demand_leaves[1:j > n_Total_leaves] = 0 			# non-existing leaves (post transition) have no demand


		# internodes produced before reproductive transition have no demand
		demand_internodes = with(sink_strengths$Internode,strength * demand_function(current_internode_TT,To,ao,bo))
		demand_internodes[is.na(demand_internodes)] = 0

		total_demand = demand_root + sum(demand_leaves) + sum(demand_internodes)

		
		# allocate C to each organ
		current_root_biomass           = current_root_biomass + demand_root * Q / total_demand
		current_leaf_biomass[1:j]      = current_leaf_biomass[1:j] + demand_leaves * Q / total_demand
		current_internode_biomass[1:j] = current_internode_biomass[1:j] + demand_internodes * Q / total_demand

		# store current state in time series
		Q_j[j] = Q / ( TT_by_phytomer[j] / mean(TT_by_phytomer)) 	# normalize Q roughly / day because of variable length phytomers
		D_j[j] = total_demand 
		S_j[j] = S
		leaf_biomass[1:j,j] = current_leaf_biomass	
		internode_biomass[1:j,j] = current_internode_biomass	
		leaf_demand[1:j,j] = demand_leaves	
		root_biomass[j] = current_root_biomass

		# increment ages of leaves and internodes
		cum_TT = sum(TT_by_phytomer[1:j])
		current_leaf_TT = current_leaf_TT + TT_by_phytomer[j]
		current_internode_TT = current_internode_TT + TT_by_phytomer[j]
	}


	output = list()
	output$leaf_biomass      = leaf_biomass
	output$internode_biomass = internode_biomass
	output$root_biomass      = root_biomass
	output$TT_by_phytomer    = TT_by_phytomer
	output$Q_j               = Q_j
	output$D_j               = D_j
	output$S_j               = S_j
	output$leaf_demand       = leaf_demand

	return(output)
}

# ----- Compare to results in paper ---- #

output = run_mod_base(
					reproductive_transtion_TT,
					RUE_per_phytomer,
					Phytomer_initiation_rate_fun,
					sink_strengths,
					PAR_umol_s,
					Daylength               
				  ) 
leaf_biomass      = output$leaf_biomass     
internode_biomass = output$internode_biomass
TT_by_phytomer    = output$TT_by_phytomer  


# load paper data	
y_pixels = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Fig4b_Y.txt",h=T)
x_pixels = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Fig4b_X.txt")
leaf_mass = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Fig4b_mass.txt")
y_lm = lm(Y~X,y_pixels)
x_lm = lm(X.1~X,x_pixels)
leaf_mass$Phytomer = round((leaf_mass$X-coef(x_lm)[1])/coef(x_lm)[2])
leaf_mass$Leaf_mass = (leaf_mass$Y-coef(y_lm)[1])/coef(y_lm)[2]

plot(NA,NA,xlim=c(0,45),ylim=c(0,max(c(leaf_biomass,leaf_mass$Leaf_mass),na.rm=T)))
col=0
for(tt in unique(leaf_mass$oCd)){
	col = col+1
	cycle = min(which(tt <= cumsum(TT_by_phytomer)))-1  # why I need to subtract 2 here is a bit strange. Maybe the don't count the first 2 cycles?
	print(cycle)
	i = leaf_mass$oCd == tt
	lines(1:cycle,leaf_biomass[1:cycle,cycle],col=col)
	points(leaf_mass$Phytomer[i],leaf_mass$Leaf_mass[i],col = col)
}

# note, the TT:693 timepoint doesn't work right because I haven't implemented the branching or fruiting.
