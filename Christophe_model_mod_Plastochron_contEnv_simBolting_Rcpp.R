# This modifies the Christophe model to more appropriatly (I think) model the plastochron as photosynthesis dependent based on Chenu et al 2005

# To do this, I have to make two main adjustments:
	# 1) add a pre-emergence period when the first few plastochrons are initiated
	# 2) add a delay for each leaf pre-expansion. This seems to be necessary because the beta function isn't flexible enough to have a long early period of slow growth
	# 3) also, TT_by_phytomer can't be adjusted until the end of the current cycle because it relies on S.

# This closely follows the original model in output, but the TT_emergence should be optimized. 
	# with some fiddling, it works pretty well. But not sure if the values for TT_emergence and leaf_delay are reasonable.
# alternatively, it might be better to alter the RUE and ao/bo parameters to re-estimate the parameters
	# but, if I go to Chew's model, this could be replaced by Rasse and Tocquin
# Now, adding bolting. 
	# Once a threshold is crossed, leaves stop being produced (no more phytomers)
	# At this point, the internodes are activated and expand based on their sink strengths / ages
	# This continues for a specified number of days, or until the total shaft biomass reaches a threshold

nDays = 200
Grnd_Tmp = rep(19.3,nDays*24)
PAR_umol_h = 1.0*rep(c(rep(1.6 * 8/1.6 * 1e6 / 12/60/60,13)*c(.1,rep(1,11),.9),rep(0,11)),nDays) * 60 * 60 	# in umol_h
Hours_light = rep(c(c(.1,rep(1,11),.9),rep(0,11)),nDays)

# Set up parameters
Q0 = 1.6e-5			

leaf_lifespan = 560			# oCd
nCycles = 180
n_Total_leaves = 45

base_temp = 3				# oC
air_temp = 19.3				# oC
TU_per_day = (air_temp - base_temp)


TT_per_hour = (Grnd_Tmp - base_temp)/24

min_PIR = 1/30.3 			# /oCd Based on Juv_phytomer
max_PIR = 1/5    			# /oCd Generally doesn't seem to matter. May be too fast
leaf_width_length_ratio = 0.5

# leaf delay and emergence parameters
TT_emergence = 5*TU_per_day		# Their "emergence plant" in Figure 1A looks at least 5+ days old
leaf_delay = 150
internode_delay = 50
threshold_bolt_mass = Inf

sink_strengths = list(
	Cotyledon_leaf = list(
		strength = 1,
		ao = 3.07,
		bo = 5.59,
		To = 300),	
	Rosette_leaf = list(
		strength = 1,
		ao = 3.07,
		bo = 5.59,
		To = 400),	
	Root = list(
		strength = 2.64,
		ao = 13.03,
		bo = 9.58,
		To = 960),		
	Inflorescence_leaf = list(
		strength = 0.41,
		ao = 3.07,
		bo = 5.59,
		To = 400),
	Internode = list(
		strength = 0.69,
		ao = 2.62,
		bo = 2.98,
		To = 357)	
	)

RUE_per_phytomer = approxfun(x=c(355,406,507,693,980),y=c(2.71,2.71,4.56,1.84,2.37)*.95,method='linear',rule=2)
	
parameters = list(
	sink_strengths            = sink_strengths,
	min_PIR                   = min_PIR,
	max_PIR                   = max_PIR,
	leaf_width_length_ratio   = leaf_width_length_ratio,
	TT_emergence              = TT_emergence,
	Q0                        = Q0,
	TU_per_day                = TU_per_day,
	leaf_lifespan             = leaf_lifespan,
	leaf_delay                = leaf_delay,
	internode_delay           = internode_delay,
	RUE_per_phytomer          = RUE_per_phytomer,
	nCycles                   = nCycles,
	threshold_bolt_mass       = threshold_bolt_mass
	)	

# RUE is dynamic with age. Text says linear interpolation. Changes with Reproductive transition
# Dropping the list here gives a huge speedup in C++

library(Rcpp)

simulate_veg_repro_growth = function(parameters, TT_per_hour,PAR_umol_h) {
	# 1) simulates plant growth and development until the end of the environmental vectors.
	# 2) simulates plants bolting on each day (after day 4)
	# 3) returns leaf biomass and length, root biomass, internode biomass, stalk biomass for each plant (bolting day) on each observation day
	require(Rcpp)
	sourceCpp('Rcpp_functions_contEnv_simBolting_testing.cpp')	

	nCycles = parameters$nCycles
	nDays = length(TT_per_hour)/24

	# Calculate plant growth curve assuming no bolting. So just Cauline and Rosette leaves.
		veg_growth_curve = c_sim_vegetative_development(
							nDays,
							nCycles,
							RUE_per_phytomer = parameters$RUE_per_phytomer,
							parameters,
							TT_per_hour,
							PAR_umol_h                   
							   ) 


	# Now, on each day, simulate plant growth assuming bolting occurred
		nDays_leafProduction = sum(veg_growth_curve$TT_by_day>0)
		leaf_biomass         = array(NA,dim=c(nCycles,nDays,nDays_leafProduction-1))
		internode_biomass    = array(NA,dim=c(nCycles,nDays,nDays_leafProduction-1))
		root_biomass         = array(NA,dim=c(nDays,nDays_leafProduction-1))
		leaf_length          = array(NA,dim=c(nCycles,nDays,nDays_leafProduction-1))
		stalk_biomass        = array(NA,dim=c(nDays,nDays_leafProduction-1))

		threshold_bolt_mass = parameters$threshold_bolt_mass
		# leaves_vs_bolting_day = c()

		for(bolting_day in 4:(nDays_leafProduction-1)){
			# find nCycles at bolting_day: number of leaves with current_leaf_TT > 0
			initial_cum_TT = veg_growth_curve$TT_by_day[bolting_day]
			initial_leaf_TT = pmax(veg_growth_curve$current_leaf_TT - veg_growth_curve$current_leaf_TT[1]+initial_cum_TT,0)
			nLeaves = sum(initial_leaf_TT>0)

			# pull out plant initial condition at this date
			initial_leaf_biomass = c(veg_growth_curve$leaf_biomass[1:nLeaves,bolting_day])
			initial_root_biomass = c(veg_growth_curve$root_biomass[bolting_day])

			# leaves_vs_bolting_day = rbind(leaves_vs_bolting_day,data.frame(
			# 																bolting_day = bolting_day, 
			# 																nLeaves = nLeaves,
			# 																nZeroLeaves = sum(initial_leaf_biomass==0),
			# 																nCaulineLeaves = sum(initial_leaf_TT[1:nLeaves]<internode_delay)
			# 															  ))

			reproducive_growth_curve = c_sim_reproductive_development(
																		initial_leaf_biomass,
																		initial_root_biomass,
																		initial_leaf_TT[1:nLeaves],
																		initial_cum_TT,
																		bolting_day+1,
																		threshold_bolt_mass,
																		RUE_per_phytomer = parameters$RUE_per_phytomer,
																		parameters,
																		TT_per_hour,
																		PAR_umol_h
																	   ) 
			# Keep leaf and root sizes up through bolting day
			leaf_biomass[1:nLeaves,1:bolting_day,bolting_day]      = veg_growth_curve$leaf_biomass[1:nLeaves,1:bolting_day]
			root_biomass[1:bolting_day,bolting_day]                = veg_growth_curve$root_biomass[1:bolting_day]

			#add leaf, internode and root sizes after bolting_day
			leaf_biomass[1:nLeaves,(bolting_day+1):nDays,bolting_day]      = reproducive_growth_curve$leaf_biomass[,(bolting_day+1):nDays]
			internode_biomass[1:nLeaves,(bolting_day+1):nDays,bolting_day] = reproducive_growth_curve$internode_biomass[,(bolting_day+1):nDays]
			root_biomass[(bolting_day+1):nDays,bolting_day]                = reproducive_growth_curve$root_biomass[(bolting_day+1):nDays]

			# calculate leaf lengths and internode biomass
			final_leaf_TT = c(veg_growth_curve$TT_by_day[1:bolting_day],reproducive_growth_curve$TT_by_day[-c(1:bolting_day)])
			leaf_length[1:nLeaves,,bolting_day] = sapply(1:nDays,function(day) {
				cleaf_length(leaf_biomass[1:nLeaves,day,bolting_day],TT = final_leaf_TT[day],width_length_ratio = parameters$leaf_width_length_ratio)
				})
			if(bolting_day < (nDays-1)) {
				stalk_biomass[(bolting_day+1):nDays,bolting_day] = colSums(internode_biomass[1:nLeaves,(bolting_day+1):nDays,bolting_day])
			} else {
				stalk_biomass[(bolting_day+1):nDays,bolting_day] = 0
			}
	}

	output = list()
	output$leaf_biomass      = leaf_biomass
	output$root_biomass      = root_biomass
	output$internode_biomass = internode_biomass
	output$leaf_length       = leaf_length
	output$stalk_biomass     = stalk_biomass
	output$TT_by_day         = c(veg_growth_curve$TT_by_day[1:nDays_leafProduction],reproducive_growth_curve$TT_by_day[(nDays_leafProduction+1):nDays])
	output$TT_by_phytomer    = veg_growth_curve$TT_by_phytomer

	return(output)
}


# find # leaves greater than min_LL_thresh on each observation day given each bolting day
min_LL_thresh = 3
num_visible_leaves = apply(leaf_length,2,function(lls) colSums(lls>min_LL_thresh,na.rm=T))
filled.contour(1:(nDays_leafProduction-1),1:nDays,num_visible_leaves,xlab = 'bolting_day',ylab = 'observation_day')
perc_max_leaves = apply(num_visible_leaves,2,function(x) x/max(x)*100)
perc_max_leaves[lower.tri(perc_max_leaves)] = NA
filled.contour(1:(nDays_leafProduction-1),1:nDays,perc_max_leaves,xlab = 'bolting_day',ylab = 'observation_day')

image(1:200,1:113,(total_internode_biomass>0.005)+1);abline(-13,1)



leaf_biomass      = veg_growth_curve$leaf_biomass     
TT_by_phytomer    = veg_growth_curve$TT_by_phytomer 
TT_by_day         = veg_growth_curve$TT_by_day  


# load paper data	
y_pixels = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Fig4b_Y.txt",h=T)
x_pixels = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Fig4b_X.txt")
leaf_mass = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Fig4b_mass.txt")
y_lm = lm(Y~X,y_pixels)
x_lm = lm(X.1~X,x_pixels)
leaf_mass$Phytomer = round(predict(x_lm,list(X=leaf_mass$X)))
leaf_mass$Leaf_mass = (leaf_mass$Y-coef(y_lm)[1])/coef(y_lm)[2]
# leaf_mass = leaf_mass[leaf_mass$oCd != 693,]
leaf_mass$oCd = leaf_mass$oCd + TT_emergence

plot(NA,NA,xlim=c(0,45),ylim=c(0,max(c(leaf_biomass,leaf_mass$Leaf_mass),na.rm=T)))
col=0
for(tt in unique(leaf_mass$oCd)){
	col = col+1
	day = min(which(tt <= TT_by_day))+1  # why I need to subtract 2 here is a bit strange. Maybe the don't count the first 2 cycles?
	print(day)
	cycle = sum(!is.na(leaf_biomass[,day]))
	if(cycle == 0) next
	i = leaf_mass$oCd == tt
	lines(1:cycle,leaf_biomass[1:cycle,day],col=col)
	points(leaf_mass$Phytomer[i],leaf_mass$Leaf_mass[i],col = col)
}
# note, the TT:693 timepoint doesn't work right because I haven't implemented the branching or fruiting.


# compare number of visible leaves to prediction

# calculate leaf lengths
leaf_areas = leaf_lengths = array(NA,dim = dim(leaf_biomass))
for(j in 1:nCycles){
	leaf_areas[,j] = SLA_by_TT(sum(TT_by_phytomer[1:j]))*leaf_biomass[,j]*100^2
	leaf_lengths[,j] = leaf_length(leaf_biomass[,j],sum(TT_by_phytomer[1:j]))
}
visible_size = 1
leaves_visible = apply(leaf_lengths,2,function(x) sum(x>visible_size,na.rm=T))

y_pixels = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/data_Y.txt",h=T)
x_pixels = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/data_X.txt")
actual_visible_leaves = read.delim("~/Dropbox/DER_projects/Leaves_vs_flowers/Christophe/Christophe_data.txt")
y_lm = lm(Y~X,y_pixels)
x_lm = lm(X.1~X,x_pixels)
actual_visible_leaves$oCd = (actual_visible_leaves$X-coef(x_lm)[1])/coef(x_lm)[2]
actual_visible_leaves$Visible_Leaves = (actual_visible_leaves$Y-coef(y_lm)[1])/coef(y_lm)[2]
actual_visible_leaves$oCd = actual_visible_leaves$oCd + TT_emergence

plot(cumsum(TT_by_phytomer)[1:nCycles],1:nCycles)
points(actual_visible_leaves$oCd,actual_visible_leaves$Visible_Leaves,col=2)
points(cumsum(TT_by_phytomer[1:nCycles]),leaves_visible,col=3)

