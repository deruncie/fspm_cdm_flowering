#include <Rcpp.h>
using namespace Rcpp;





// [[Rcpp::export]]
double c_photosynthesis( double  CO2,
                            double Tleaf,
                            double PAR,
                            double vlmax25,
                            int daylength,
                            NumericVector p       // global parameters
                            )
{

    //Rasse and Tocquin, 2006

    //Calculate rate of photosynthesis limited by Rubisco
    //___________________________________________________
    //___________________________________________________


    //Rubisco activity for CO2
    //________________________

    double R = p[39-1]; //gas constant
    double act_ener_kc25 = p[40-1]; //activation energy for kc25 (kJ mol-1)
    double KmRubisco25 = p[41-1]; //Michaelis-Menten constant of Rubisco for CO2 at 25(Pa)

    double numC = act_ener_kc25*(Tleaf-25);
    double denomC = 298*R*(Tleaf+273);

    double kc_Tleaf = KmRubisco25*exp(numC/denomC); //M-M Rub co2 at leaf T


    //Rubisco activity for O2
    //_______________________

    double act_ener_ko25 = p[42-1]; //activation energy for ko25
    double KmRubO = p[43-1]; //Michaelis-Menten constant of Rubisco for O2 at 25 (Pa) 

    double numO = act_ener_ko25*(Tleaf-25);

    double ko_Tleaf = KmRubO*exp(numO/denomC); //M-M constant for O2 at leaf T


    //Photosynthetic Rubisco capacity at leaf T
    //_________________________________________

    double act_ener_vlmax = p[44-1]; //activation energy for vlmax25

    double numR = act_ener_vlmax*(Tleaf-25);

    double vlmax_Tleaf = vlmax25*exp(numR/denomC); //micromol CO2 m-2 s-1


    //Effective M-M constant of Rubisco (Pa)
    //______________________________________

    double o2_parpre = p[45-1]; //O2 partial pressure (Pa)

    double kprim_effective = kc_Tleaf*(1+o2_parpre/ko_Tleaf);


    //Rubisco-limited rate
    //____________________

    double co2_comp_point = p[46-1]+p[47-1]*(Tleaf-25)+ p[48-1]*pow((Tleaf-25),2); //CO2 compensation point in the absence of mitochondrial respiration (Pa)
    double int_co2 = p[49-1]*CO2; //intercell CO2 partial pressure (Pa)

    double numAV = int_co2-co2_comp_point;
    double denomAV = int_co2+kprim_effective;

    double av_rub = vlmax_Tleaf*(numAV/denomAV);



    //Calculate rate of photosynthesis limited by RuBP regeneration
    //_____________________________________________________________
    //_____________________________________________________________


    //Electron transport
    //__________________

    double act_ener_jm25 = p[50-1]; //activation energy for jm25 (kJ mol-1)
    double h_curvature = p[51-1]; //Curvature parameter of Jm (J mol-1)
    double s_elec = p[52-1]; //Electron transport temperature response parameter (J K-1 mol-1)

    double numt1 = act_ener_jm25*(Tleaf-25);
    double denomt1 = 298*R*(Tleaf+273);
    double term1 = exp(numt1/denomt1);

    double numt2 = s_elec*298-h_curvature;
    double denomt2 = R*298;
    double term2 = 1+exp(numt2/denomt2); //intermediate calculation for jm

    double numt3 = s_elec*(273+Tleaf)-h_curvature;
    double denomt3 = R*(273+Tleaf);
    double term3 = 1+exp(numt3/denomt3); //intermdeiate calculation fro jm

    double jv = 2.1;
    switch (daylength) {
        case 4 :
            jv = 2.1; //assuming plateau
            break;
        case 6 :
            jv = 2.1; //assuming plateau
            break;
        case 8 :
            jv = 2.1; //default value in Rasse and Tocquin or p[53-1]
            break;
        case 12 :
            jv = 1.7; //from Flexas et al (2007)
            break;
        case 14 :
            jv = 1.4; //from Bunce (2008)
            break;
        case 18 :
            jv = 1.2; //extrapolated 
            break;
    }        
         

    //jm25_pot = p[53-1]*vlmax25; //Potential rate of electron transport per unit leaf area at 25 (micromol m-2 s-1)
    double jm25_pot = jv*vlmax25;

    double jm_pot = jm25_pot*term1*term2/term3; //Potential rate of electron transport per unit leaf area (micromol m-2 s-1)


    //Photosystem II
    //______________

    double fspec = p[54-1]; //Spectral correction factor. PAR absorbed by tissue other than the chloroplast lamella

    double ile_par = PAR*(1-fspec)/2; //PAR effectively absorbed by PSII

    double cc = ile_par*jm_pot;
    double bb = -1*(ile_par+jm_pot);


    //jl electron transport (quadratic solution)
    //__________________________________________

    double thetal_curve = p[55-1]; //Curvature of leaf response of electron transport to irradiance
    double aa = thetal_curve;

    double rho = pow(bb,2)-4*aa*cc;

    double sol = 0;
    if (rho <= 0 ) {
        sol = 0;
    } else {
        sol = (-1*bb-pow(rho,0.5))/(2*aa); //Solving the quadratic equation thetal*j^2 - (ile+jm)*j + ile*jm = 0
    }


    //Rubisco regeneration
    //____________________

    double numAJ = sol*(int_co2-co2_comp_point);
    double denomAJ = 4*(int_co2+2*co2_comp_point);

    double aj_RuBP = numAJ/denomAJ; //Rate of photosynthesis limited by RuBP
        
    double net_rate = 0;
    if (rho <= 0 ) {
        net_rate = av_rub;
    } else {
        net_rate = std::min(av_rub,aj_RuBP); //Net rate of leaf photosynthesis (micromol CO2 m-2 leaf s-1)
    }

    return net_rate;
}


// [[Rcpp::export]]
List c_mainres(   double Tleaf,
                double leaf_c,
                double root_c,
                double suc_c_perplant,
                double rosette_area,
                double timestep,
                NumericVector p       // global parameters
                ) {

    //Calculation for maintenance respiration
    //_______________________________________
    //_______________________________________

    double act_ener_res20 = p[56-1]; //activation energy for rl 20 (kJ/mol)

    double suc_conc_s = suc_c_perplant/rosette_area;  

    double rl20_leafres = (p[57-1]*suc_conc_s + p[58-1])*24; //Leaf respiration at 20 (g CO2 C/m2/day)

    double numres = act_ener_res20*(Tleaf - 20);
    double denomres = 293*p[39-1]*(Tleaf + 273);

    double  rl_leafres = 0;
    if (suc_conc_s <= 0 ) {
        
        rl_leafres = 0;
    } else {
        rl_leafres = rl20_leafres*exp(numres/denomres); //leaf respiration at leaf temperature (g CO2 C/m2/day)
    }

    double leaf_res = rl_leafres*rosette_area*timestep; //leaf respiration per plant per time step (gC per plant per time step)

    double root_res = leaf_res*root_c/leaf_c; //gC/plant/time step

    return List::create( _["leaf_res"] = leaf_res,
                         _["root_res"] = root_res);
}

// [[Rcpp::export]]
List c_assimilation( int daylength,
                     int is_light,
                     double net_rate,
                     double timestep,
                     double leaf_res,
                     double root_res,
                     double suc_c_perplant,
                     double rosette_area,
                     double sta_c_endday,
                     NumericVector   p   // global parameters
                        ) {

    double convert_to_gC = timestep*p[59-1]*pow(10.,-6)*12; //conversion factor for micromol/m2 leaf/sec to gC/m2 leaf/timestep

    //Baseline conversion coefficient (default p[60-1]=0.125)
    double sta_base = p[60-1]; //Baseline starch conversion coefficient

    //Starch turnover (default p[61-1]=0.84)
    double sta_convert_night = 0;
    if (daylength ==18) {
        sta_convert_night = 0.6; //Based on observation in Sulpice et al (2013)
    } else    {
        sta_convert_night = p[61-1];
    }

    double sta_use = 0;
    double al_pt_plant_assim = 0;
    double suc_sta_base = 0;
    double al_suc = 0;
    if (    is_light == 1 ){        
            sta_use = 0; //conversion of starch to sugar
            al_pt_plant_assim = net_rate*rosette_area*convert_to_gC; //Assimilatory flux per plant (gC/plant/timestep)
            suc_sta_base = al_pt_plant_assim*sta_base; //baseline starch conversion
            al_suc =al_pt_plant_assim - suc_sta_base;
    } else {
            al_pt_plant_assim = 0;
            sta_use = ((sta_c_endday*sta_convert_night)/(24-daylength))*timestep*24; //Dark conversion of starch to sugar
            suc_sta_base = 0;
            al_suc = 0;
    }

    double suc_equi = p[62-1]; //equilibrium sucrose plus hexose concentration in leaves (g C/m2 leaf)
    double suc_c_interm = suc_c_perplant + sta_use + al_suc - root_res - leaf_res;

    double current_value = suc_c_interm - (suc_equi*rosette_area);

    double suc_c_disp = 0;
    if (    current_value <= 0 ) {
            suc_c_disp = 0;
    } else {
            suc_c_disp = current_value; //Amount of sugar available for growth
    }     

    return List::create( _["suc_sta_base"] = suc_sta_base,
                         _["sta_use"] = sta_use,
                         _["suc_equi"] = suc_equi,
                         _["al_suc"] = al_suc,
                         _["suc_c_disp"] = suc_c_disp,
                         _["suc_c_interm"] = suc_c_interm,
                         _["al_pt_plant_assim"] = al_pt_plant_assim);
}


// [[Rcpp::export]]
List c_organdemand(double timestep,
                 double rsratio,
                 double leaf_c,
                 NumericVector p  // global parameters
                        ){


    //Leaf growth
    //___________

    double maxgrowth = p[63-1]; 
    double leaf_growth = maxgrowth*leaf_c*timestep; //Maximal leaf growth (gC/plant/timestep)

    double rc_coef = p[64-1]; //Growth respiration to total growth allocation

    double numrlc = leaf_growth*rc_coef;
    double denomrlc = 1-rc_coef;

    double rlc_pt = numrlc/denomrlc; // Growth respiration In g C/plant/timestep (required)


    //Root growth
    //___________

    double root_growth = leaf_growth*rsratio; //Root growth (g C/plant/time step)

    double numrrc = root_growth*rc_coef;
    double denomrrc = 1-rc_coef;

    double rrc_pt = numrrc/denomrrc; // Growth respiration In g C/plant/timestep (required)


    //Total growth demand
    //___________________

    double totdem = leaf_growth + root_growth + rlc_pt + rrc_pt;

    return List::create( _["rrc_pt"] = rrc_pt,
                         _["totdem"] = totdem,
                         _["rlc_pt"] = rlc_pt,
                         _["root_growth"] = root_growth,
                         _["leaf_growth"] = leaf_growth);
}


// [[Rcpp::export]]
List c_allocation( double rrc_pt,
                 double totdem,
                 double rlc_pt,
                 double root_growth,
                 double leaf_growth,
                 double suc_c_disp,
                 int is_light,
                 NumericVector p  // global parameters
                        ) {

    double suc_growth = 0;
    double rrc_pt1    = 0;
    double root_gro1  = 0;
    double rlc_pt1    = 0;
    double leaf_gro1  = 0;
    double suc_sta  = 0;

    if  (   totdem < suc_c_disp ) {
        
            suc_growth = totdem;
            rrc_pt1    = rrc_pt;
            root_gro1  = root_growth;
            rlc_pt1    = rlc_pt;
            leaf_gro1  = leaf_growth;
            
            if (is_light == 1 ) {
                suc_sta = suc_c_disp - suc_growth;
            } else {
                suc_sta = 0;
            }    

    } else  {
            suc_growth = suc_c_disp; //sucrose for growth (g C/plant)
            rrc_pt1 = rrc_pt*(suc_growth/totdem); // Root growth respiration In g C/plant/timestep (actually used)
            root_gro1 = root_growth*(suc_growth/totdem); //root growth actually achieved
            rlc_pt1 = rlc_pt*(suc_growth/totdem); // Leaf growth respiration In g C/plant/timestep (actually used)
            leaf_gro1 = leaf_growth*(suc_growth/totdem); //leaf growth actually achieved (g C/plant/timestep)
            suc_sta = 0;
    }


    return List::create( _["rlc_pt1"] = rlc_pt1,
                         _["suc_sta"] = suc_sta,
                         _["root_gro1"] = root_gro1,
                         _["rrc_pt1"] = rrc_pt1,
                         _["leaf_gro1"] = leaf_gro1);
}



// [[Rcpp::export]]
List c_translocation( double rosette_area,
                    double suc_c_interm,
                    double suc_equi,
                    double leaf_c,
                    double root_c,
                    NumericVector p   // global parameters
                            ) {


    double suc_equi_plant = suc_equi*rosette_area; //Equilibrium sucrose plus hexose mass for whole plant (g C/plant)
    double root_and_leaf_c = root_c + leaf_c; //Total root and leaf C (g C/plant)

    double leaf_trans = 0;
    double root_trans = 0;
    if  (   suc_c_interm <= suc_equi_plant ) {   //translocation needed
            
            double scalingl = leaf_c/root_and_leaf_c;
            leaf_trans = (suc_equi_plant - suc_c_interm)*scalingl; //Translocation from leaves (gC/plant)
            double scalingr = root_c/root_and_leaf_c;
            root_trans = (suc_equi_plant - suc_c_interm)*scalingr; //Translocation from root (g C/plant)
            
    } else {
            leaf_trans = 0;
            root_trans = 0;
    }        


    return List::create( _["leaf_trans"] = leaf_trans,
                         _["root_trans"] = root_trans);
}